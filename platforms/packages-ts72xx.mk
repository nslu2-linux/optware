# Packages that *only* work for ts72xx - do not just put new packages here.
SPECIFIC_PACKAGES = 

# Packages that do not work for ts72xx.
BROKEN_PACKAGES = \
	asterisk \
	ldconfig \
	py-bazaar-ng py-simplejson \
	qemu qemu-libc-i386 slrn spandsp 

